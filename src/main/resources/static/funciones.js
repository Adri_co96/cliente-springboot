function eliminar(id){
swal({
  title: "¿Estas seguro de borrar el elemento",
  text: "No se eliminó el registro",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    swal("Registro no eliminado!", {
      icon: "success",
    });
  } else {
    swal("Your imaginary file is safe!");
  }
});
}