package com.cliente.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

//desarrollo de modelo entidad 

@Entity
@Table(name="CLIENTE")
public class Cliente {
	@Id
	@Column(name="ID")
	@GeneratedValue
	private Integer id;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="NACIMIENTO")
	private Date nacimiento;
	@Column(name="NSS")
	private String nss;
	@Column(name="CURP")
	private String curp;
	@Column(name="CORREO")
	private String correo;
	@Column(name="TELEFONO")
	private String telefono;
	
	//SUPER CLASE
	public Cliente() {
		super();
	}
	
	public Cliente(Integer id) {
		//super();
		//this.id=id;
    }
	
	public Cliente(
			Integer id,
			String nombre,
			Date nacimiento,
			String nss,
			String curp,
			String correo,
			String telefono) {
	
	super();
	this.id=id;
	this.nombre=nombre;
	this.nacimiento=nacimiento;
	this.nss=nss;
	this.curp=curp;
	this.correo=correo;
	this.telefono=telefono;
	
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
