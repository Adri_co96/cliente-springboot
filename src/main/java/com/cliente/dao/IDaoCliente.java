package com.cliente.dao;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cliente.modelo.Cliente;

@Repository
public interface IDaoCliente extends CrudRepository<Cliente, Serializable>{

}
//
//INTERFAZ NO SE PROGRAMA, ESTA INTERFAZ PARA EL ACCESO A JDBC, HIBERNATE
//CRUD REPOSITORY: SAVE, DELETE, SELECTBYID,UPDATE
