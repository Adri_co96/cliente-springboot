package com.cliente.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import com.cliente.modelo.Cliente;
import com.cliente.service.IClienteService;

//clase para ser de tipo controlador
@Controller
public class ClienteController {
	
	//traer los metodos de la clase de servicio que ya estan implementados en la clase Service
	@Autowired
	private IClienteService service;
	
	//realizar el mapeo para poder listar todos los objetos clientes de la BD, y te los refresa hacia el index.html
	@GetMapping("/listar")
	public String listarReporte(Model model) {
		List<Cliente>clientes=service.listClientes();
		model.addAttribute("clientes",clientes);
		return "index";
	}
	
	//realiza 
	@GetMapping("/new")
	public String guardarCliente(Model model) {
		model.addAttribute("cliente",new Cliente());
		return "form";
	}
	
	@PostMapping("/save")
	public String saveCliente(Cliente cliente) {
		service.save(cliente);
		return"redirect:/listar";
	}
	
	@GetMapping("editar/{id}")
	public String editarReporte(@PathVariable int id,Model model) {
		Optional<Cliente> cliente=service.getClienteById(id);
		model.addAttribute("cliente",cliente);
		return "form";
	}
	
	@GetMapping("/eliminar/{id}")
	public String eliminarReporte(@PathVariable int id) {
		service.delete(id);
		return "redirect:/listar";
	}

}
