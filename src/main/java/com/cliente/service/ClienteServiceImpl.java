package com.cliente.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cliente.dao.IDaoCliente;
import com.cliente.modelo.Cliente;

//clase para ser de tipo servicio
@Service
public class ClienteServiceImpl implements IClienteService {

	//creacion de objeto dao para llamar a hiberneta para relizar operaciones tipo crud
	@Autowired
	private IDaoCliente dao;
	
	//metodo para retornar toda la lista de clientes
	@Override
	public List<Cliente> listClientes() {
		// TODO Auto-generated method stub
		return (List<Cliente>)dao.findAll();
	}

	//metodo para retornar un objeto cliente por id
	@Override
	public Optional<Cliente> getClienteById(int id) {
		// TODO Auto-generated method stub
		return dao.findById(id);
	}

	//metodo para guardar un nuevo registro
	@Override
	public int save(Cliente cliente) {
		int result=0;
		Cliente c=dao.save(cliente);
		if(!c.equals(null))
			result=1;
		return result;
	}
	
	//metodo para eliminar un registro cliente por id
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		dao.deleteById(id);

	}

}
