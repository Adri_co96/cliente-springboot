package com.cliente.service;

import java.util.List;
import java.util.Optional;

import com.cliente.modelo.Cliente;

public interface IClienteService {
	
	//DECLARACION DE METODOS 
	public List<Cliente> listClientes();
	public Optional<Cliente>getClienteById(int id);
	public int save(Cliente cliente);
	public void delete(int id);
	

}
